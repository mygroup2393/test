FROM httpd
COPY index.html /usr/local/apache2/htdocs/
COPY second.html /usr/local/apache2/htdocs/
EXPOSE 80
